# Crawl a site $1 and put log in $2


# Call as Eg $ ./CrawlSite.sh http://www.interpressnews.ge/en/  IPN-crawl.log
crawlurl=$1
log=$2

# -N only fetch files not seen before (this often does not work, eg when pages are generated from a CMS, and timespamping is not configured well on their server
# -nc no-clobber, do not re-download the same file 
# -p get all images and stuff as well
# -r recusrive
# --no-parent : do not go up the directory stucture
# wait and limit rate are to be nice to the website
# -U pretends that we are Mozilla
# I like to write the output not th stdout, but to the log file....
wget --wait=4 --limit-rate=30K -r --no-parent   -N    -U Mozilla "$crawlurl"   >>  "$log" 2>&1

