#!/bin/sh


## LOGBOOK: copied this to plus2:/scratch/marx/GeorgiaNews/IPN, but java xquery does not run there, so cannot install the crontab there with this script.


#http://www.interpressnews.ge/ge/dghis-yvela-siakhle.html?view=allnews&start=50


#paths
#saxon="/Users/admin/marx/bin/saxon9.jar";  #linux: "/home/marx/bin/saxon9.jar"
#outdir="/Users/admin/Documents/TIGeorgia/GeorgianNewsSites/IPN/NewsOfTheDay";  # On mac: /Users/admin/Documents/TIGeorgia/GeorgianNewsSites/IPN/NewsOfTheDay"  plus2 "/scratch/marx_plus/GeorgiaNews/georgiannewsanalyser/IPN/NewsOfTheDay";


cd $(dirname $0)  # This command ensures that relative paths work, also in crontabs.

source GetNewsOfThisDay.config

GetNewsOfThisDay="$outdir/GetNewsOfThisDay.xquery";

today=`date "+%Y-%m-%d"`


url="http://www.interpressnews.ge"
 
dataCSV="$outdir/DownloadedNews.csv"
ExtractedDataCSV="$outdir/ExtractedNews.csv"
ExtractedDataXML="$outdir/ExtractedNews.xml"
NewExtractedDataCSV="$outdir/NewExtractedNews.csv"
NewExtractedDataXML="$outdir/NewExtractedNews.xml"
BadurlsCSV="$outdir/Badurls.csv";


GEurl="/ge/dghis-yvela-siakhle.html"
ENurl="/en/all-the-news-of-the-day.html"
RUurl="/ru/all-news-day.html"
 
 
touch "$BadurlsCSV"; # to avoid complaining when it is empty.
touch "$ExtractedDataCSV"; # to avoid complaining when it is empty.

# Initialze to an empty file and add the header to $NewExtractedDataCSV
echo "id	source	language	downloaddate	publication_time	publication_date	article_title	article_text" > "$NewExtractedDataCSV";
rm "$NewExtractedDataXML"


### Get the pages with content
for u in $GEurl $ENurl $RUurl; 
    do 
    #NOT Used anymore language=`echo $u| sed 's%/%% ;s%/.*%%'`
    pages=`curl   --limit-rate 30K "${url}${u}" | tr '\n' ' '| grep -o "$u[^\"]*" |sort|uniq|sed "s%^%$url%"`
    # get URLs that need to be downloaded and write to $dataCSV
    for page in $pages;
    do
        curl   --limit-rate 30K  "$page" |
        tr '\n' ' '| 
         grep -o -P '<a[^<]*(Read more|Подробнее|სრულად)' |  # here you find the urls to the news articles in all three languages
         sed "s%^[^\"]*\"%%;s/\".*//"  |   # get the url (ie remove stuff around it)
         sed '/http/!s%^%http://www.interpressnews.ge%' >> "$dataCSV" # | #   if the url is already absolute, if it is then do not prefix with $url (Had to replace $url by its value, because cannot use ! inside double quotes (and double quotes are needed for the expansion of )$url
       #NOT Used anymore  sed "s/$/,$today,$language/" >> "$dataCSV"
         
    done
    done
    
# Sort and uniq the $dataCSV
cat "$dataCSV"|sort|uniq > "$dataCSV-effe";mv "$dataCSV-effe" "$dataCSV";
    

    
# Now downlaod   and Extract 
 

# Extract the candidate URLs from $dataCSV
# 

# Only take those urls from $dataCSV that are not yet in $ExtractedDataCSV and not in the $BadurlsCSV 
downloadurls=`cat "$BadurlsCSV"  "$ExtractedDataCSV" | awk -F$"\t" '{print $1}' |sort |sed '/^$/d' | comm -2 -3 "$dataCSV" - ` 
 
# Now download and extract immediately. We do not store the raw HTML files

# NEW DATA is written to seperate files $NewExtractedDataXML and $NewExtractedDataCSV and then concatenated to the "big data files". 
# This is done to facilitate easy incremental updating of the SOLR index.
for thisurl in $downloadurls;
    do
     echo $thisurl
     language=`echo $thisurl| sed "s%$url/%% ;s%/.*%%"`
     downloaddate=`date "+%Y-%m-%dT%H:%M:%S"`  # valid xsd:datetime
     curl --silent  --limit-rate 30K "$thisurl" |
     #wget -O- --wait=4 --limit-rate=30K "$thisurl" | # alternative for curl
     tidy -q -raw -utf8 -n -asxml    | 
     sed '1,2d' >  "$outdir/tempie"  ;  # remove the doctype decaration, and thus don't go to the web to colelct the DTD. This is very slow on plus2.
     # SAXON: -s: input xml file  (this can be - if it is stdin)  -q: input query file
     # csv output
      
      
     java  -cp "$saxon" net.sf.saxon.Query -s:"$outdir/tempie" -q:"$GetNewsOfThisDay"  out="csv" downloaddate="$downloaddate" language="$language" source="$url" url="$thisurl" >> $NewExtractedDataCSV;
     cat $NewExtractedDataCSV >>  "$ExtractedDataCSV" ;
     # xml output
     java  -cp "$saxon" net.sf.saxon.Query -s:"$outdir/tempie" -q:"$GetNewsOfThisDay"  out="xml" downloaddate="$downloaddate" language="$language" source="$url" url="$thisurl" >> $NewExtractedDataXML;
     cat  $NewExtractedDataXML >>  "$ExtractedDataXML"  ;
     # NOTE  we extract using XPath and XQuery . This can be done much faster with xsltproc
     sleep 1  # be nice to the server
    done
 
 # store urls for which extraction failed  (we will nor download them again)
 cat $ExtractedDataCSV | awk -F$"\t" '{print $1}' |sort  | comm -2 -3 "$dataCSV" -  >> "$BadurlsCSV"   
 # Sort and uniq the $dataCSV
cat "$BadurlsCSV"|sort|uniq > "$BadurlsCSV-effe";mv "$BadurlsCSV-effe" "$BadurlsCSV";
 
# Cron command 
# Do the download every 12 hours
#50 */12 * * * $outdir/GetNewsOfThisDay.sh

 
  
