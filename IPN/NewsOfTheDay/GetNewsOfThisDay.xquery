declare namespace gna="http://www.politicalmashup.nl/GeorgianNewsAggregator";

(: declare option saxon:output "method=text";  (: output as text without xml header :)  :)
declare option saxon:output    "omit-xml-declaration=yes";
declare option saxon:output  "indent=yes";

declare variable $url  external; 
declare variable $source external;
declare variable $language external;
declare variable $downloaddate external;
declare variable $out external;

(: CSV functions :)
declare function gna:NotEmpty($s){ if ($s) then $s else 'NULL'};
declare function gna:tostring($list){normalize-space(string-join( for $t in $list return normalize-space(string($t)),' '))};
declare function gna:NoDoubleQuotes($text){replace($text,'"',"'")};
declare function gna:NoSingleQuotes($text){replace($text,"'",'"')};
declare function gna:QuotesAround($text){concat('"',gna:NoDoubleQuotes($text),'"')};
declare function gna:SingleQuotesAround($text){concat("'",gna:NoSingleQuotes($text),"'")};


declare function gna:csvOUT($data){
concat(
       string-join(for $i in $data return gna:NotEmpty(normalize-space($i)),'&#09;')
       ,
       '&#10;')
       };

declare function gna:xmlOUT($schema,$data){
<tr>
{for $i at $pos in $schema return
    <td id='{$i}'>{normalize-space($data[$pos])}</td>
}
</tr>
};

let $title := //*:meta[@name="title"]/@content
let $datetime := //*:span[@class="createdate"]
let $time := replace($datetime,'..-..-....','')  (: time and date according to the article itself :)
let $isodate := replace(replace($datetime,"..:..",""),"(..)-(..)-(....)","$3-$2-$1") 
let $text := //*:div[@id="article"]
let $data := ($url, $source, $language, $downloaddate, $time, $isodate,gna:QuotesAround( $title),gna:QuotesAround($text) ) 
let $schema := ("url","source","language","downloaddate","publication time", "publication date", "article title", "article text")
return
if ($out = 'csv') then  gna:csvOUT($data) else gna:xmlOUT($schema,$data)
       
 (: possibly create a different output format depending on the input the used search engine wants 
 
 :)      