#!/Users/admin/Library/Enthought/Canopy_64bit/User/bin/python
#
# 1) Parse RSS feeds of Parool Amsterdam 
# 2) Create a parsimonious language model of each day
# 2a) Display parsimonious model as a wordcloud
# 3) DO THIS LATER. IT REQUIRES A MORE COMPLEX DATASTRUCTURE  Make sure the words in the model link to the right articles of the Parool
#  For 3 we should make an inverted index, or use a search-engine for that. 
#  The simplest solution is to index all items from the feeds keeping the date and the feedname.
#  The link for a term T in feed F on date D is then the first hit when querying T with feed=F and date=D.
#  This could even be done offline, before publishing the wordcloud. Then you do not have anything "live with users".

import feedparser
import nltk
#from lxml import html 
from time import mktime
from datetime import date
from datetime import datetime



# Rudimentary Wordcloud printer
def wc_printer(dict):
    for (w,f) in dict.items():  # only first 100 [:100]
        print '{}:{}'.format(w.encode('utf-8'), f)

# Part 0 , the source-urls for the feeds

# Collected from http://www.parool.nl/parool/nl/106/SERVICE/integration/nmc/frameset/rss_feeds.dhtml
feeds = { "Amsterdam": "http://www.parool.nl/amsterdam/rss.xml", 
          "Cultuur": "http://www.parool.nl/cultuur/rss.xml",
           "AmsterdamPolitiek":"http://www.parool.nl/amsterdam/politiek/rss.xml"}




# Part 1, obtain the data, Run say once a day 

# In real version this should be read from disc
AmsterdamParoolSummary = {}   # Dict with feeds.keys() as attributes and values {pubDate:wordcounts} dicts for each day.

for feedtype in feeds.keys():
    f =  feedparser.parse(feeds[feedtype])
    pubDate = date.fromtimestamp(mktime(f.feed.published_parsed))  # or use datetime. if you want the time as well
    words = ''
    for item in f.entries: 
        # The next condition greatly reduces the number of words.....(well, at least in the early morning)
        if date.fromtimestamp(mktime(item.published_parsed)) == pubDate:  # only take items with the same pubdate as the channel (otherwise we keep "old" entries)
            words= words+' '+item.title+' '+item.description   # TODO: html entities / diacrieten gaan niet goed (bijv financi"en  wordt gesplitst )
                                                               # TODO: clean up/remove  interpunction
    dict = nltk.FreqDist(nltk.word_tokenize(words))  # count words 
    #wc_printer(dict) 
    AmsterdamParoolSummary[feedtype] = {pubDate:dict}
    
print AmsterdamParoolSummary  # save to disc 

# Part 2 Create the parsimonious model for each feed, for each day.
    
     
