Download files in a list of RSS feeds (here given by the input file Paroolfeeds.csv)

Result:
1) Directory News_Articles with all articles
2) a CSV file DownloadedNews.csv with the information from the RSS feed in spreasdheet format.


Do not change the two scripts RSSParse.sh and RSSparse.xquery

SETUP

1) create file  <FILE> with feeds
2)  update RSSParse.sh by setting the variable feeds to <FILE>
3)  mkdir News_Articles


TODO / PROBLEMS

1) Some feeds give errors (one cannot be downloaded, one is not valid XML)
2) Not all feeds have the same schema, so it does not make sense to parse them all without looking at the element names. You see that when you look at column 3: it contains mostly URLs, but also text data. So we need to parse them more carefully.
3) But storing the data as XML makes it more difficult to avoid duplicates. Now a simple sort|uniq because of the CSV format. If we store as XML we must keep files storing the URLS of the articles, and check those files for duplicates.




