#!/bin/sh

 # logbook
 # 2014-02-10 Op plus2:/scratch/marx_plus/ODE_Parool crontab aangezet die elk uur dit script draait.


source RSSParse.config


GetNewsOfThisDay="$outdir/RSSParse.xquery";

dataCSV="$outdir/DownloadedNews.csv"
feeds="$outdir/GeorgianMediaRSSfeeds.csv"; #ParoolFeeds.csv";
 

# call the extractor  
java  -cp "$saxon" net.sf.saxon.Query   -q:"$GetNewsOfThisDay"  feeds="`cat $feeds `" >> $dataCSV

#remove doubles
cat "$dataCSV" |sed 's/^ *//' |sort|uniq > tempie;mv tempie "$dataCSV";

## Now download all new articles 



# Step 1 Set a datadir and the memory file
Datadir="$outdir/News_Articles" 
DownloadedURLS="$outdir/DownloadedURLS.csv"
NewURLS="$outdir/NewURLS.csv"
# Initialize
touch $DownloadedURLS $NewURLS;
# Step 2
# store new urls  
cat "$dataCSV" | awk -F$"\t" '{print $3}' |sort  | comm -2 -3 - "$DownloadedURLS"   > "$NewURLS"    
# download the new urls
wget -i "$NewURLS" --directory-prefix="$Datadir" --limit-rate=30k --wait=2 --no-clobber --quiet 
# gzip all downloaded articles
gzip -f --quiet ${Datadir}/*
#  Update the list of downloaded urls
cat "$NewURLS"  "$DownloadedURLS" |sort > "$DownloadedURLS-effe"; mv "$DownloadedURLS-effe" "$DownloadedURLS";

 
 