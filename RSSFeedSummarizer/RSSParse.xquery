declare namespace gna="http://www.politicalmashup.nl/GeorgianNewsAggregator";

declare option saxon:output    "omit-xml-declaration=yes";
declare option saxon:output    "method=text";



 

declare variable $feeds external; (:  hard list example  ( "http://www.parool.nl/amsterdam/rss.xml", 
            "http://www.parool.nl/cultuur/rss.xml",
           "http://www.parool.nl/amsterdam/politiek/rss.xml"      );  :)

 

(: CSV functions :)
declare function gna:NotEmpty($s){ if ($s) then $s else 'NULL'};
declare function gna:tostring($list){normalize-space(string-join( for $t in $list return normalize-space(string($t)),' '))};
declare function gna:NoDoubleQuotes($text){replace($text,'"',"'")};
declare function gna:NoSingleQuotes($text){replace($text,"'",'"')};
declare function gna:QuotesAround($text){concat('"',gna:NoDoubleQuotes($text),'"')};
declare function gna:SingleQuotesAround($text){concat("'",gna:NoSingleQuotes($text),"'")};


declare function gna:csvOUT($data){
concat(
       string-join(for $i in $data return gna:NotEmpty((normalize-space($i))),'&#09;')
       ,
       '&#10;')
       };
       
let $feeds := tokenize($feeds,'\n')
return
       
for $feed in $feeds[. ne ''] return
for $i in doc($feed)//item  
return  gna:csvOUT(($feed,$i/*))
 